<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Votes extends Model
{
    //
    protected $fillable = [
        'user_id', 'submission_id'
    ];
    
    protected $table = "votes";
}
