<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    //
    
    protected $fillable = [
        'user_id', 'submit_model', 'invest_model', 'votenya', 'tenor, quote'
    ];
    
    protected $table = "tb_submission";
}
