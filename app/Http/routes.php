<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'frontend', 'middleware' => ['web']], function () {
    Route::get('home', 'frontendController@home');
    
    Route::get('login', 'frontendController@loginpage');
    
    Route::get('video', 'frontendController@video');
    
    Route::get('tentang-kita', 'frontendController@tentangkita');
    
    Route::get('galery', 'frontendController@galery');
    
    Route::get('simulasi-1', 'frontendController@simulasi1');
    
    Route::get('simulasi-2/{opsi}','frontendController@simulasi2');
    
    Route::get('simulasi-3/{opsi}/{jenis}', 'frontendController@simulasi3');
    
//    Route::get('addSubmit/{opsi}')
    
    Route::get('savetenor/{id}/{tenor}', 'frontendController@saveTenor');
    
    Route::get('insertstory/{id}','frontendController@insertStory');
    
    Route::post('processtory/{id}','frontendController@processStory');
    
     Route::get('simulasi-4/{id}', 'frontendController@simulasi4');
    
    Route::get('setvotes/{user_id}/{submission_id}', 'frontendController@setVotes');
    
    Route::get('profile','frontendController@goprofile');
    
    Route::get('tweetshare/{id}','frontendController@tweetshare');
   
});



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //Route::get('/', 'myController@home');
    
    Route::get('/', function(){
        return redirect('/frontend/home');
    });

Route::get('/video', 'myController@video');

Route::get('/tentang-kita', function () {
    return view('tentangkita');
});
    
Route::get('home', function () {
    return view('home');
});

//Route::get('/login', function () {
//    return view('loginreg');
//});
    
Route::get('/login', 'myController@loginpage');

Route::get('/register', function () {
    return view('auth.register');
});

    
    Route::post('/auth/login', 'frontendController@doLogin');
    Route::get('auth/logout', 'frontendController@doLogout');

    // Registration routes...
    //Route::get('auth/register', 'Auth\AuthController@getRegister');
    
    Route::post('auth/register', 'frontendController@doRegister');

    Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');
    Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');

// Authentication routes...
//Route::get('auth/login', 'Auth\AuthController@getLogin');
//Route::post('/auth/login', 'myController@doLogin');
//Route::get('auth/logout', 'myController@doLogout');
//
//// Registration routes...
////Route::get('auth/register', 'Auth\AuthController@getRegister');
//Route::post('auth/register', 'myController@doRegister');
//    
//Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');
//Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');

});
