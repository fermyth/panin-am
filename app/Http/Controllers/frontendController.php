<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\User;
use Validator;
use Session;
use Redirect;

use DB;
use App\Models\Votes;
use App\Models\Submission;

use Flash;

class frontendController extends Controller
{
    //
    
    public function home(Request $request){
        if(Auth::user()){
            return view('/frontend/home')
                ->with("login_status",1);
            
        }else{
            
            return view('/frontend/home')
                ->with("login_status",0);//,["faqr" => $faq]);
        }
        
    }
    

    
    public function video(Request $request){
       if(Auth::user()){
            return view('/frontend/frontvideo')
                ->with("login_status",1);
        }else{
            
            return view('/frontend/frontvideo')
                ->with("login_status",0);//,["faqr" => $faq]);
        }
    }
    
    public function simulasi1(Request $request){
       if(Auth::user()){
            return view('/frontend/simulasi1')
                ->with("login_status",1);
        }else{
            
            return redirect('/frontend/login')
                ->with("login_status",0);//,["faqr" => $faq]);
        }
    }
    
    public function simulasi2(Request $request){
        $opsi = $request->opsi;
       if(Auth::user()){
            return view('/frontend/simulasi2')
                ->with("login_status",1)
                ->with("opsi",$opsi);
        }else{
            
            return redirect('/frontend/login')
                ->with("login_status",0);//,["faqr" => $faq]);
        }
    }
    
    public function simulasi3(Request $request){
        $opsi = $request->opsi;
        $jenis = $request->jenis;
        
       if(Auth::user()){
           
           $user_data = Auth::user();
           
           $submits = new Submission;
           $submits->user_id = $user_data->id;
           $submits->submit_model = $opsi;
           $submits->invest_model = $jenis;
           $submits->timestamps = true;
           $submits->save();
           
            return view('/frontend/simulasi3')
                ->with("login_status",1)
                ->with("opsi",$opsi)
                ->with("submit_id", $submits->id)
                ->with("jenis", $jenis);
                
        }else{
            
            return redirect('/frontend/login')
                ->with("login_status",0);//,["faqr" => $faq]);
        }
    }
    
    public function simulasi4(Request $request){
        
        
       if(Auth::user()){
           $sid = $request->id;
           $submits = Submission::find($sid);
           
           $model = $submits->submit_model;
           
            return view('/frontend/simulasi4')
                ->with("login_status",1)
                ->with("model","frontend/img/sim4-tengah-".$model.".png")
                ->with("textnya","frontend/img/text-final/".$model."-".$submits->tenor.".png" )
                ->with("pilihan", $model)
                ->with("tenor", $submits->tenor)
                ->with("id",$sid);         
        }else{
            
            return redirect('/frontend/login')
                ->with("login_status",0);//,["faqr" => $faq]);
        }
    }
    
    public function tentangkita(Request $request){
        if(Auth::user()){
            return view('/frontend/tentangkita')
                ->with("login_status",1);
        }else{
            
            return view('/frontend/tentangkita')
                ->with("login_status",0);
        }
        
    }
    
    public function saveTenor(Request $request){
        $post_id = $request->id;
        $tenor = $request->tenor;
        
        $posts = Submission::find($post_id);
        
        if($posts){
            $posts->tenor = $tenor;
            $posts->save();
        }
        
        return redirect('/frontend/insertstory/'.$post_id)
                ->with("login_status",1);//,["faqr" =>
        
    }
    
    public function insertStory(Request $request){
        
        $post_id = $request->id;
        
        return view('frontend.simulasiquote')
        ->with("submit_id", $post_id);
    }
    
    public function processStory(Request $request){
        $post_id = $request->id;
        
        $story = $request->story;
        
        $posts = Submission::where("id",$post_id)->first();
        
        if($posts){
            $posts->quote = $story;
            $posts->save();
        }
        
        return redirect('/frontend/simulasi-4/'.$post_id)
                ->with("login_status",1);//,["faqr" =>
    }
    
    public function galery(Request $request){
        if(Auth::user()){
            //$user_data = Auth::user();
            //$user_data = Auth::user();
            $allsubmit = DB::table('tb_submission')
                ->join('users', 'users.id', '=', 'tb_submission.user_id')
                ->select('*', 'tb_submission.id as submit_id')
                ->where('tb_submission.quote','<>',' ')
                ->orderBy('tb_submission.id', 'desc')
                ->paginate(8);
            
            return view('/frontend/galery' , ["dummy" => $allsubmit])
                ->with("login_status",1);
        }else{
            
            $allsubmit = DB::table('tb_submission')
                ->join('users', 'users.id', '=', 'tb_submission.user_id')
                ->select('*', 'tb_submission.id as submit_id')
                ->where('tb_submission.quote','<>',' ')
                ->orderBy('tb_submission.id', 'desc')
                ->paginate(8);
            
            return view('/frontend/galery' , ["dummy" => $allsubmit])
                ->with("login_status",0);
        }
        
    }
    
    public function goprofile(Request $request){
       if(Auth::user()){
                $user_data = Auth::user();
            $mysubmit = Submission::where("user_id",$user_data->id)
                ->limit(4)
                ->get();
            //Debugbar::info($mysubmit);
            return view('/frontend/profile', ["dummy" => $mysubmit])
                ->with("login_status",1);
        }else{
            
            return redirect('/frontend/home')
                ->with("login_status",0);//,["faqr" => $faq]);
        }
    }
    
    public function loginpage(Request $request){
        if(Auth::user()){
            return redirect('/frontend/profile')
                ->with("login_status",1);
        }else{
            
            return view('/frontend/loginreg')
                ->with("login_status",0);//,["faqr" => $faq]);
        }
    }
    
    
    public function doLogin(Request $request){
        $userdata = array(
        'email'     => $request->input('email'),
        'password'  => $request->input('password')
        );
        
        if (Auth::attempt($userdata)) {
            $user_data = Auth::user();
        
        return redirect('/frontend/profile')
            ->with("login_status",1);

    } else {        

        // validation not successful, send back to form 
        Flash::error('Wrong username/password combination.');
        return redirect('/frontend/login')
            ->with("login_status",0);
//         return Redirect::back()->withInput()->withFlashMessage('Wrong username/password combination.')
//             ->with("login_status",0);

    }
        
    }
    
    public function doLogout(Request $request){
        
        Auth::logout();
        Session::flush();
        return redirect('/frontend/login')
            ->with("login_status",0);
    }
    
    public function doRegister(Request $request){
        /**
		* validation
		*/
		$validator = Validator::make($request->all(),
		[
			'name' => 'required',
			'password' => 'required',
			'email' => 'required|unique:users|email',
			
		], $messages = [
			'required' => 'The :attribute field is required',
			'email.unique' => 'The Email Address you submit is already registered',
			'email' => 'The :attribute not a valid email address'
		]);
		if($validator->fails()) {

			return response()->json(['status'=>500,'data'=>$validator->errors()->all()]);
		}
        
        $user = new User;
		$user->name = $request->input('name');
        $user->password = bcrypt($request->input('password'));//Hash::make('secret')
		$user->email = $request->input('email');
		$user->timestamps = true;
		$user->save();
        
        Auth::loginUsingId($user->id);
        
        return redirect('/frontend/profile')
            ->with("login_status",1);
        
    }
    
//    public function addVotes($user_id,$submission_id){
//       
//        
//    }
    
//    $ch = RWMUsersChallenge::where("fi_challenges_id",$challenge->fi_challenges_id)->where("user_id",$user->user_id)->first();
    
    public function setVotes(Request $request){
        $user_id = $request->user_id;
        $submission_id = $request->submission_id;
//        $hasil = addVotes($user_id, $submission_id);
        
        $oldvotes = Votes::where("user_id", $user_id)->where("submission_id", $submission_id )->first();
        
        if($oldvotes){
            // sudah ada yang vote
            $hasil =  false;
        }else{
            // belum ada yang votes
            $newvotes = new Votes;
            $newvotes->user_id = $user_id;
            $newvotes->submission_id = $submission_id;
            $newvotes->timestamps = true;
            $newvotes->save();
            
            $updateVotes = Submission::where("id", $submission_id )->first();
            if($updateVotes){
                $nilai = $updateVotes->votenya;
                $nilai = $nilai + 1;
                $updateVotes->votenya=$nilai;
                $updateVotes->save();
            }       
            $hasil =  true;
        }
        
        if($hasil){
            Flash::success('Terima kasih atas vote yang diberikan.');
            return redirect('/frontend/galery')
            ->with("login_status",1);
        }else{
            Flash::error('Maaf, anda telah mem-vote pilihan ini sebelumnya.');
            return redirect('/frontend/galery')
            ->with("login_status",1);
        }
    }
    
    public function tweetshare(Request $request){
        
        
        $post_id = $request->id;
        $posts = Submission::where("id",$post_id)->first();
        
        if(Auth::user()){
            return view('frontend.twittershare',["dummy" => $posts] )
                ->with("login_status",1);
            
        }else{
            
            return view('frontend.twittershare',["dummy" => $posts] )
                ->with("login_status",0);//,["faqr" => $faq]);
        }
        
        
    }
    
    
}
