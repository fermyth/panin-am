<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\User;
use Validator;
use Session;
use Redirect;

use Flash;

class myController extends Controller
{
    //
    
    public function home(Request $request){
        if(Auth::user()){
            return redirect('/video')
                ->with("login_status",1);
        }else{
            
            return view('homepage')
                ->with("login_status",0);//,["faqr" => $faq]);
        }
        
    }
    
    public function video(Request $request){
       if(Auth::user()){
            return view('/video')
                ->with("login_status",1);
        }else{
            
            return redirect('/login')
                ->with("login_status",0);//,["faqr" => $faq]);
        }
    }
    
    public function loginpage(Request $request){
        if(Auth::user()){
            return view('/video')
                ->with("login_status",1);
        }else{
            
            return view('loginreg')
                ->with("login_status",0);//,["faqr" => $faq]);
        }
    }
    
    public function doLogin(Request $request){
        $userdata = array(
        'email'     => $request->input('email'),
        'password'  => $request->input('password')
        );
        
        if (Auth::attempt($userdata)) {
            $user_data = Auth::user();
        
        return redirect('/video')
            ->with("login_status",1);

    } else {        

        // validation not successful, send back to form 
        Flash::error('Wrong username/password combination.');
        return redirect('/login')
            ->with("login_status",0);
//         return Redirect::back()->withInput()->withFlashMessage('Wrong username/password combination.')
//             ->with("login_status",0);

    }
        
    }
    
    public function doLogout(Request $request){
        
        Auth::logout();
        Session::flush();
        return redirect('/login')
            ->with("login_status",0);
    }
    
    public function doRegister(Request $request){
        /**
		* validation
		*/
		$validator = Validator::make($request->all(),
		[
			'name' => 'required',
			'password' => 'required',
			'email' => 'required|unique:users|email',
			
		], $messages = [
			'required' => 'The :attribute field is required',
			'email.unique' => 'The Email Address you submit is already registered',
			'email' => 'The :attribute not a valid email address'
		]);
		if($validator->fails()) {

			return response()->json(['status'=>500,'data'=>$validator->errors()->all()]);
		}
        
        $user = new User;
		$user->name = $request->input('name');
        $user->password = bcrypt($request->input('password'));//Hash::make('secret')
		$user->email = $request->input('email');
		$user->timestamps = true;
		$user->save();
        
        Auth::loginUsingId($user->id);
        
        return redirect('/video')
            ->with("login_status",1);
        
    }
}

