<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.headmeta')
</head>

<body>
    @include('includes.navbar')

    <div class="middlehome middletentang">
        <div class="row">
        
            <div class="col-lg-8 kiri">
                <div class="tentangjudul">
                    <h3>#iAmPlanned, demi masa depan finansial yang baik!</h3>
                </div>
                <div class="tentangket col-lg-8">
                    <p>
                        Apapun impian Anda, semua bisa terwujud <br/>
                        dengan perencanaan yang baik dan tepat.  
                    </p>
                    <p>
                        Melalui #iAmPlanned, ayo mulai rencanakan masa depan<br/> 
                        finansial dan wujudkan impian Anda bersama <br/>
                        Panin Asset Management.
                    </p>
                </div>
            </div>
            
            <div class="col-lg-4 kanan">
<!--
                <img src="img/btn_registrasi.png" /> <br />
                <a href="/video"><img src="{{ URL::asset('img/btn_video.png')}}" /></a>
-->
            </div>
            
        </div>
        
    </div>
    @include('includes.footer')
</body>
</html>
