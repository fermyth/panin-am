<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.fheadmeta')
</head>

<body>
    @include('frontend.fnavbar')

    <div class="middlehome middlesimulasi1 middlesimulasi3">
        <div class="row">
        
            <div class="col-lg-5 kiri">
                <div class="tentangjudul">
                    <img  src="{{ URL::asset('frontend/img/text-simulasi.png')}}" />
                </div>
                
                <div class="formnya">
                    <form class="form-horizontal" role="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Total Goal:</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" id="pv" name="pv" placeholder="" value="1,000,000,000">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-4" for="pwd">Jangka Waktu Investasi:</label>
                        <div class="col-sm-6"> 
                          <select id="nper" class="selectpicker">
<!--                                <option>5</option>-->
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                              <option>6</option>
                              <option>7</option>
                              <option>8</option>
                              <option>9</option>
                              <option>10</option>
                              <option>11</option>
                              <option>12</option>
                              <option>13</option>
                              <option>14</option>
                              <option>15</option>
                              <option>16</option>
                              <option>17</option>
                              <option>18</option>
                              <option>19</option>
                              <option>20</option>
                              <option>-</option>
                              <option>20</option>
                                
                    </select>
                        </div>
                        <div class="col-sm-2"> 
                            <label class="control-label col-sm-4" for="pwd">Tahun</label>
                          </div>
                      </div>
                        
                        <div class="form-group">
                        <label class="control-label col-sm-4" for="pwd">Asumsi Inflasi:</label>
                        <div class="col-sm-8"> 
                          <select id="asinlfasi" class="selectpicker">
                            <option>5%</option>
                            <option>6%</option>
                            <option>7%</option>
                            <option>8%</option>
                            <option>9%</option>
                            <option>10%</option>
                        </select>
                        </div>
                      </div>
                        
                        <div class="form-group">
                        <label class="control-label col-sm-4" for="pwd">Asumsi Return:</label>
                        <div class="col-sm-8"> 
                          <select id="asreturn" class="selectpicker">
                                <option>7%</option>
                                <option>8%</option>
                                <option>9%</option>
                                <option>10%</option>
                                <option>11%</option>
                                <option>12%</option>
                                <option>13%</option>
                                <option>14%</option>
                                <option>15%</option>
                                <option>16%</option>
                                <option>17%</option>
                                <option>18%</option>
                                <option>19%</option>
                                <option>20%</option>
                    </select>
                        </div>
                      </div>

                      
                    </form>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                          <button onclick="hitung();" type="text" class="btn btn-default">Kalkulasi</button>
                        </div>
                      </div>
                </div>
                
            </div>
            <div class="col-lg-7 kanan">           
                <div class="text-judul">
                    <i>"Rencanakan dan Hitung Investasimu"</i>
                </div>
                
                <div class="row sim1-menus">
                <div class="selectnya col-lg-8">
                 <div class="formnya">
                    <form class="form-horizontal" role="form"  method="POST" action="#">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-group">
                        <label class="control-label col-sm-6" for="email">Perkiraan Goal di Masa Mendatang:</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="val_FV" name="val_FV" placeholder="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-6" for="pwd">Investasi sekaligus :</label>
                        <div class="col-sm-6"> 
                          <input type="text" class="form-control" id="val_PV" name="val_PV" placeholder="">
                        </div>
                      </div>
                        
                        <div class="form-group">
                        <label class="control-label col-sm-6" for="pwd">Investasi Bulanan:</label>
                        <div class="col-sm-6"> 
                         <input type="text" class="form-control" id="val_PMT" name="val_PMT" placeholder="">
                        </div>
                      </div>
                    </form>
                </div>  

                </div>
                
            <div class="sim1-lanjut col-lg-4">
                <a href="#">                    
                    <img onclick="javascript:selanjutnya('{{$submit_id}}')" src="{{ URL::asset('frontend/img/text-lanjutkan.png')}}" />

                </a>
                    </div>
                    
                </div>
                
               <div class="tag-pilihan2">
                    <img  src="{{ URL::asset('frontend/img/sim3-tengah-orang.png')}}" />
                </div> 
                
                
                <div class="icon-bawah">
                    <img  src="{{ URL::asset('frontend/img/simulasi3.png')}}" />
                </div>
                
            </div>           
        </div>
 
    </div>
    @include('frontend.ffooter')
    
    <script type="text/javascript">
        function selanjutnya(id){
            var tenor = $('#nper').find("option:selected").text();
            window.location.href = "/frontend/savetenor/"+id+"/"+tenor;
        }
    </script>
    
</body>
</html>
