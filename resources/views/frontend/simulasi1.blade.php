<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.fheadmeta')
</head>

<body>
    @include('frontend.fnavbar')

    <div class="middlehome middlesimulasi1 middlesimulasi">
        <div class="row">
        
            <div class="col-lg-4 kiri">
                <div class="tentangjudul">
                    <img  src="{{ URL::asset('frontend/img/text-simulasi.png')}}" />
                </div>
                
            </div>
            <div class="col-lg-8 kanan">           
                <div class="text-judul">
                    <i>"Tentukan Mimpimu bersama Panin Asset Management"</i>
                </div>
                
                <div class="tag-pilihan">
                    <img id="tulisan" src="{{ URL::asset('frontend/img/sim1-pilihan-Mobil.png')}}" />
                </div>
                <div class="row sim1-menus">
                <div class="selectnya col-lg-7">
                    <select id="mySelect" class="selectpicker">
                      <option>Mobil</option>
                      <option>Rumah</option>
                      <option>Pendidikan</option>
                      <option>Liburan</option>
                      <option>Pensiun</option>
                    </select>

                </div>
                
                <div class="sim1-lanjut col-lg-4"><a href="#">                    <img onclick="pindahketiga();" src="{{ URL::asset('frontend/img/text-lanjutkan.png')}}" />
</a></div>
                    
                </div>
                <div class="tag-pilihan2">
                    <img id="tengah"  src="{{ URL::asset('frontend/img/sim1-tengah-mobil.png')}}" />
                </div>
                
                <div class="icon-bawah">
                    <img  src="{{ URL::asset('frontend/img/simulasi1.png')}}" />
                </div>

            </div>
            

            
        </div>
        
        
        
    </div>
    @include('frontend.ffooter')
</body>
</html>
