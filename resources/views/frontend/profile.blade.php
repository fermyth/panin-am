<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.fheadmeta')
</head>

<body>
    @include('frontend.fnavbar')

    <div class="middlehome middleprofile">
        <div class="row">
        
            <div class="col-lg-4 kiri">
                <div class="tentangjudul">
                    <img  src="{{ URL::asset('frontend/img/text-profil.png')}}" />
                </div>
                
            </div>
            <div class="col-lg-8 kanan">           
                <div class="row profpic">
                    <div class="col-lg-2 komponen">
                        
                        <div class="icons">
                            <img src="{{ URL::asset('/frontend/img/icon/ic-profile.png')}}" />
                        </div>
                        
                    </div>
                    
                    <div class=" col-lg-6 profname">
                        {{ Auth::user()->name }}
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-lg-offset-6 col-lg-5">
                       <a href="/frontend/simulasi-1"><img height="80px" src="{{ URL::asset('frontend/img/text-startnew.png')}}" /></a>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-offset-7 col-lg-4">
                        <img  src="{{ URL::asset('frontend/img/text-profil2.png')}}" />
                    </div>
                </div>
                
<!--                Bagian 2 

-->
                
                <div class="row">
<!--                    data 1 -->
                    @foreach($dummy as $k => $v)
                    <div class="col-lg-2 komponen">
                        <div class="nama">
                            {{ Auth::user()->name }}
                        </div>
                        <div class="icons">
                            <img src="{{ URL::asset('/frontend/img/icon/ic-'.$v['submit_model'].'.png')}}" />
                        </div>
                        <div class="row votes">
                            <div class="col-lg-6">
                                <img src="{{ URL::asset('frontend/img/btn-vote.png')}}" />
                            </div>
                            <div class="col-lg-6 votescount">
                                {{$v['votenya']}} votes
                            </div>
                        </div>
                    </div>
                    @endforeach
<!--                 end data 1   -->
                    
                </div>
                
<!--                 end bagian 2   -->
                
                

            </div>
            

            
        </div>
        
        
        
    </div>
    @include('frontend.ffooter')
</body>
</html>
