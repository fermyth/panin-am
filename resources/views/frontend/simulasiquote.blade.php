<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.fheadmeta')
</head>

<body>
    @include('frontend.fnavbar')

    <div class="middlehome middlesimulasi1 middlesimulasi3">
        <div class="row">
        
            <div class="col-lg-5 kiri">
                <div class="tentangjudul">
                    <img  src="{{ URL::asset('frontend/img/text-simulasi.png')}}" />
                </div>
                
                
                
            </div>
            <div class="col-lg-7 kanan">           
                <div class="text-judul">
                    <i>"Masukkan cerita tentang rencanamu"</i>
                </div>
                
                <div class="row sim1-menus">
                <div class="selectnya col-lg-8">
                 <div class="formnya">
                     <form id="myform" class="form-horizontal" role="form"  method="POST" action="/frontend/processtory/{{$submit_id}}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <div class="form-group">
                        
                        <div class="col-sm-12">
                          <textarea name="story" style="text-align:left" rows="6" class="form-control"></textarea>
                            
                        </div>
                         <label class="control-label col-sm-12" for="email">Maksimum 200 karakter</label>
                      </div>
                     </form>
                    
                </div>  

                </div>
                
            <div class="sim1-lanjut col-lg-4">
                <a href="#">                    
                    <img onclick="javascript:selanjutnya()" src="{{ URL::asset('frontend/img/text-lanjutkan.png')}}" />

                </a>
                    </div>
                    
                </div>
                
               <div class="tag-pilihan2">
                    <img  src="{{ URL::asset('frontend/img/sim3-tengah-orang.png')}}" />
                </div> 
                
                
                <div class="icon-bawah">
                    <img  src="{{ URL::asset('frontend/img/simulasi3.png')}}" />
                </div>
                
            </div>           
        </div>
 
    </div>
    @include('frontend.ffooter')
    
    <script type="text/javascript">
        function selanjutnya(){
            // submit form.
            document.forms["myform"].submit();
        }
    </script>
    
</body>
</html>
