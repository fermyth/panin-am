<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.fheadmeta')
</head>

<body>
    @include('frontend.fnavbar')

    <div class="middlehome middlesimulasi1 middlesimulasi4">
        <div class="row">
        
            <div class="col-lg-4 kiri">
                <div class="tentangjudul">
                    <img  src="{{ URL::asset('frontend/img/text-simulasi.png')}}" />
                </div>
                
            </div>
            <div class="col-lg-8 kanan">           
<!--
                <div class="text-judul">
                    <i>"Rencanakan dan Hitung Investasimu"</i>
                </div>
-->
                <div class="tulisan-final"> 
                <img  src="{{ URL::asset($textnya)}}" />
                </div>
                <div class="row sim1-menus">
                <div class="selectnya col-lg-7">
                    

                </div>
                
                <div class="sim1-lanjut col-lg-4">                   <img  src="{{ URL::asset('frontend/img/share.png')}}" />

                    <a href="https://www.facebook.com/dialog/share?app_id=1531034803868314
&display=popup&href=http://iamplanned.com/frontend/img/share/{{$pilihan}}-{{$tenor}}.png&redirect_uri=http://iamplanned.com/frontend/home&picture=http://iamplanned.com/frontend/img/share/{{$pilihan}}-{{$tenor}}.png"><img  src="{{ URL::asset('frontend/img/share-fb.png')}}" />
</a>
                    <a href="https://twitter.com/intent/tweet?url=http://iamplanned.com/frontend/tweetshare/{{$id}}" target="_blank"><img  src="{{ URL::asset('frontend/img/share-tw.png')}}" />
</a>
                    
                    </div>
                    
                </div>
                
               <div class="tag-pilihan2">
                    <img  src="{{ URL::asset($model)}}" />
                </div> 
                
                
                <div class="icon-bawah">
                    <img  src="{{ URL::asset('frontend/img/simulasi4.png')}}" />
                </div>

                
                
            </div>
            

            
        </div>
        
        
        
        
    </div>
    @include('frontend.ffooter')
</body>
</html>
