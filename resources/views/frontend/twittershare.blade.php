<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="iamplanned.com">
<meta name="twitter:title" content="#iamplanned dari Panin Asset Management">
<meta name="twitter:description" content="Rencanakan masa depanmu dan share ceritamu di http://iamplanned.com">
<meta name="twitter:creator" content="@creator_username">
<meta name="twitter:image:src" content="http://iamplanned.com/frontend/img/share/{{$dummy->submit_model}}-{{$dummy->tenor}}.png">
<meta name="twitter:domain" content="iamplanned.com">
    
    
    @include('frontend.fheadmeta')
</head>

<body>
    @include('frontend.fnavbar')

    <div class="middlehome">
        <div class="row">
        
            <div class="col-lg-5 col-sm-12 kiri homelogin">
                @if(isset($login_status))
                    @if($login_status==0)
                    @include('auth.homelogin')
                @else
                   <a href="/frontend/profile" style="color:white; font-size:20px;">Hello {{ Auth::user()->name }}, </a>
                 @endif
                @endif
            </div>
            
            <div class="col-lg-7 col-sm-12 kanan">
                <div class="tulisan">
                    <img src="{{ URL::asset('frontend/img/text-front.png')}}" />
                </div>
                <a href="/frontend/video"><img src="{{ URL::asset('frontend/img/btn-vid.png')}}" /></a>
<!--                <img src="img/btn_registrasi.png" /> <br />-->
                <a href="/frontend/simulasi-1"><img src="{{ URL::asset('frontend/img/btn-plan.png')}}" /></a>
            </div>
            
        </div>
        
    </div>
    @include('frontend.ffooter')
</body>
</html>
