<nav class="navbar navbar-inverse navbar-fixed-top myheader" role="navigation">
        <div class="container whiteband">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand " href="#"><img src="/frontend/img/logo.png" width="100%" /></a>
            </div>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li {{{ (Request::is('frontend/home') ? 'class=active' : '') }}} ><a href="/frontend/home">Beranda</a></li>
                     @if(isset($login_status))
                    @if($login_status==1)
                    <li {{{ (Request::is('frontend/profile') ? 'class=active' : '') }}}><a href="/frontend/profile">Profil</a></li>
                     @endif
                @endif
                    <li {{{ (Request::is('frontend/tentang-kita') ? 'class=active' : '') }}}><a href="/frontend/tentang-kita">Tentang Kita</a></li>
                    <li><a href="/frontend/galery">Galeri</a></li>
                
                
                @if(isset($login_status))
                    @if($login_status==1)
                    
                    
                    
                    <li><a href="/auth/logout">Keluar</a></li>
                    @endif
                @endif
                </ul>
                
                
            </div><!--.nav-collapse -->
        </div>
    </nav>