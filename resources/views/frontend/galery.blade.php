<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.fheadmeta')
</head>

<body>
    @include('frontend.fnavbar')

    <div class="middlehome middlegaleri">
        <div class="row" style="text-align:left;margin-top:10px;">
                @include('flash::message')
        </div>
        <div class="row">
        
            <div class="col-lg-4 kiri">
                <div class="tentangjudul">
                    <img  src="{{ URL::asset('frontend/img/text-galery.png')}}" />
                </div>
                
            </div>
            <div class="col-lg-8 kanan">           
                <div class="row">
                    @foreach($dummy as $k => $v)
                    <div class="col-lg-2 komponen">
                        <div class="nama">
                            {{$v->name}}
                        </div>
                        <div class="icons" style="color:black">
                            <img   data-trigger="hover" data-toggle="modal" data-target="#myModal{{$v->submit_id}}" title="story" data-placement="top" data-content="{{$v->quote}}" src="{{ URL::asset('/frontend/img/icon/ic-'.$v->submit_model.'.png')}}" />
                        </div>
                        <div class="row votes">
                            <div class="col-lg-6">
                            <?php
                                    if(Auth::user()){ ?>
                                <a href="/frontend/setvotes/{{ Auth::user()->id }}/{{$v->submit_id}}">
                                    <?php
                                    }else{ ?>
                                    <a href="/frontend/login">
                                    <?php } ?>
                                <img src="{{ URL::asset('frontend/img/btn-vote.png')}}" /></a>
                            </div>
                            <div class="col-lg-6 votescount">
                                {{$v->votenya}} votes
                            </div>
                        </div>
                    </div>
                        
                    <!-- Modal -->
                    <div id="myModal{{$v->submit_id}}" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="color:black !important">{{$v->name}}'s Story</h4>
                          </div>
                          <div class="modal-body" style="color:black !important">
                            <p>{{$v->quote}}</p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>

                      </div>
                    </div>
                    @endforeach  
                </div>
                
                <div class="btn-next">
<!--                    <a href="/frontend/simulasi-1"><img  src="{{ URL::asset('frontend/img/text-startnew.png')}}" /></a>-->
                    
                    {!! $dummy->render() !!}
                    
                </div>

            </div>
            

            
        </div>
        
        
        
    </div>
    @include('frontend.ffooter')
    <script>
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover(); 
        });
        </script>
</body>
</html>
