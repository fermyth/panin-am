<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.fheadmeta')
</head>

<body>
    @include('frontend.fnavbar')
    
    <div class="middlehome middlelogin">
        <div class="row" style="text-align:left;margin-top:10px;">
                @include('flash::message')
        </div>
        <div class="row">
        
            <div class="col-lg-5 kiri">
                @include('auth.login')
            </div>
            <div class="col-lg-1 ">
                
            </div>
            <div class="col-lg-6 kanan">
                @include('auth.register')
            </div>
            
        </div>
        
    </div>
    @include('frontend.ffooter')
</body>
</html>
