<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.fheadmeta')
</head>

<body>
    @include('frontend.fnavbar')

    <div class="middlehome middletentang">
        <div class="row">
        
            <div class="col-lg-4 kiri">
                <div class="tentangjudul">
                    <img  src="{{ URL::asset('frontend/img/text-tentangkita.png')}}" />
                </div>
                
            </div>
            <div class="col-lg-8 kanan">
                <div class="tentangket">
                    <p>
                        Apapun impian Anda, semua bisa terwujud 
                        dengan perencanaan yang baik dan tepat. Melalui #iAmPlanned, ayo mulai rencanakan masa depan<br/> 
                        finansial dan wujudkan impian Anda bersama 
                        Panin Asset Management. 
                    </p>
                    <p>
                        #iAmPlanned berusaha membantu anda dalam mewujudkan impian anda mulai dari menentukan apa impian anda, kapan mimpi anda ingin direalisasikan,<br /> 
hingga seberapa besar investasi yang anda butuhkan 
                    </p>
                </div>
                
                <div class="btn-next">
                    <a href="/frontend/simulasi-1"><img  src="{{ URL::asset('frontend/img/text-startnew.png')}}" /></a>
                </div>
            </div>
            

            
        </div>
        
    </div>
    @include('frontend.ffooter')
</body>
</html>
