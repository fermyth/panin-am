<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.fheadmeta')
</head>

<body>
    @include('frontend.fnavbar')

    <div class="middlehome middlesimulasi1 middlesimulasi2">
        <div class="row">
        
            <div class="col-lg-4 kiri">
                <div class="tentangjudul">
                    <img  src="{{ URL::asset('frontend/img/text-simulasi.png')}}" />
                </div>
                
            </div>
            <div class="col-lg-8 kanan">           
                <div class="text-judul">
                    <i>"Sesuaikan Jangka dan pilihan Reksa Danamu"</i>
                </div>
                
                <div class="tag-pilihan row">
                    <div class="col-lg-4">
                        <a href="/frontend/simulasi-3/{{$opsi}}/1"><img  src="{{ URL::asset('frontend/img/sim2-opsi1.png')}}" /></a>
                    </div>
                    
                    <div class="col-lg-4">
                        <a href="/frontend/simulasi-3/{{$opsi}}/2"><img  src="{{ URL::asset('frontend/img/sim2-opsi2.png')}}" /></a>
                    </div>
                    
                    <div class="col-lg-4">
                        <a href="/frontend/simulasi-3/{{$opsi}}/3"><img  src="{{ URL::asset('frontend/img/sim2-opsi3.png')}}" /></a>
                    </div>
                </div>
<!--
                <div class="row sim1-menus">
                
                <div class="col-lg-8"></div>
                <div class="sim1-lanjut col-lg-4" ><a href="#">                    <img  src="{{ URL::asset('frontend/img/text-lanjutkan.png')}}" />
</a></div>
                    
                </div>
-->
                <div class="tag-pilihan2">
                    <img  src="{{ URL::asset('frontend/img/sim2-tengah-orang.png')}}" />
                </div>
                
                <div class="icon-bawah">
                    <img  src="{{ URL::asset('frontend/img/simulasi2.png')}}" />
                </div>

            </div>
            

            
        </div>
        
        
        
    </div>
    @include('frontend.ffooter')
</body>
</html>
