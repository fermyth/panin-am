<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.headmeta')
</head>

<body>
    @include('includes.navbar')
    
    <div class="middlehome middlelogin">
        <div class="row" style="text-align:left;margin-top:10px;">
                @include('flash::message')
        </div>
        <div class="row">
        
            <div class="col-lg-5 kiri">
                @include('auth.login')
            </div>
            <div class="col-lg-1 ">
                
            </div>
            <div class="col-lg-6 kanan">
                @include('auth.register')
            </div>
            
        </div>
        
    </div>
    @include('includes.footer')
</body>
</html>
