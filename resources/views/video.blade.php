<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.headmeta')
</head>

<body>
    @include('includes.navbar')

    <div class="middlehome middlevideo">
<!--        <div class="judul">VIDEO</div>-->
        <div class="row">
        
            <div class="col-lg-12">
                <div class="video">
                    <span class="judulvideo">Lasting Partnership</span> <br/>
                    <iframe src="https://www.youtube.com/embed/Rf6EXQTcf5A" frameborder="0" allowfullscreen></iframe>
                </div>
                
            </div>
            
            
            
        </div>
        
    </div>
    @include('includes.footer')
</body>
</html>
