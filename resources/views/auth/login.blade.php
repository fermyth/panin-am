<form class="form-horizontal" role="form"  method="POST" action="/auth/login">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-group">
    <label class="control-label col-sm-2" for="email">Email:</label>
    <div class="col-sm-8">
      <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="pwd">Password:</label>
    <div class="col-sm-8"> 
      <input type="password" class="form-control" id="pwd" name="password" placeholder="Enter password">
    </div>
  </div>

  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Login</button>
        <div class="ors" >OR</div>
       <a href="/auth/facebook"><img class="btn-default btn-fb" src="{{ URL::asset('frontend/img/btn-fb.png')}}" /></a> 
    </div>
  </div>
</form>


