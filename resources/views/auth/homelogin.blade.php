<form class="form-horizontal" role="form"  method="POST" action="/auth/login">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-group">
    <label class="control-label col-sm-2" for="email">Email:</label>
    <div class="col-sm-8">
      <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Email">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="pwd">Password:</label>
    <div class="col-sm-8"> 
      <input type="password" class="form-control" id="pwd" name="password" placeholder="Masukkan Password">
    </div>
  </div>

  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Login</button>
        <div class="ors" >ATAU</div>
       <a href="/auth/facebook"><img class="btn-default btn-fb" src="{{ URL::asset('frontend/img/btn-fb.png')}}" /></a> 
        <div class="ors" >ATAU</div>
        <a href="/frontend/login"><div type="default" class="btn btn-default btn-success">Daftar Baru</div></a>
    </div>
  </div>
</form>


