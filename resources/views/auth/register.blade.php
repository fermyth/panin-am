<!-- resources/views/auth/register.blade.php -->

<!--
<form method="POST" action="/auth/register">
    {!! csrf_field() !!}

    <div>
        Name
        <input type="text" name="name" >
    </div>

    <div>
        Email
        <input type="email" name="email" >
    </div>

    <div>
        Password
        <input type="password" name="password">
    </div>

    <div>
        Confirm Password
        <input type="password" name="password_confirmation">
    </div>

    <div>
        <button type="submit">Register</button>
    </div>
</form>
-->

<form class="form-horizontal" role="form"  method="POST" action="/auth/register">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-group">
    <label class="control-label col-sm-4" for="name">Name:</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
    </div>
  </div>    

  <div class="form-group">
    <label class="control-label col-sm-4" for="email">Email:</label>
    <div class="col-sm-6">
      <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-4" for="pwd">Password:</label>
    <div class="col-sm-6"> 
      <input type="password" class="form-control" id="pwd" name="password" placeholder="Enter password">
    </div>
  </div>
    
  <div class="form-group">
    <label class="control-label col-sm-4" for="pwd">Confirm&nbsp;Password:</label>
    <div class="col-sm-6"> 
      <input type="password" class="form-control" id="pwd" name="password_confirmation" placeholder="Confirm password">
    </div>
  </div>    
<!--
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label><input type="checkbox"> Remember me</label>
      </div>
    </div>
  </div>
-->
  <div class="form-group"> 
    <div class="col-sm-offset-3 col-sm-9">
      <button type="submit" class="btn btn-default btn-register">Register</button>
    </div>
  </div>
</form>