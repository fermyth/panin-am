<nav class="navbar navbar-inverse navbar-fixed-top myheader" role="navigation">
        <div class="container whiteband">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand " href="#"><img src="img/logo.png" width="100%" /></a>
            </div>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li {{{ (Request::is('/') ? 'class=active' : '') }}} {{{ (Request::is('video') ? 'class=active' : '') }}}><a href="/">Home</a></li>
                    <li {{{ (Request::is('tentang-kita') ? 'class=active' : '') }}}><a href="/tentang-kita">Tentang Kita</a></li>
<!--                    <li><a href="#contact">Galeri</a></li>-->
                </ul>
            </div><!--.nav-collapse -->
        </div>
    </nav>