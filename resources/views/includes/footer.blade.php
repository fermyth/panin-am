<footer class="navbar-fixed-bottom footers">
<!--        <div class="row">-->
            <img class="logobawah" src="{{ URL::asset('img/logobawah.png')}}" width="15%" />
<!--        </div>-->
    </footer>

    <script src="{{ URL::asset('js/jquery-1.11.1.min.js')}}"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js')}}"></script>
    <script>
        $('#flash-overlay-modal').modal();
    </script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75641405-1', 'auto');
  ga('send', 'pageview');

</script>