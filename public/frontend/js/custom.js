$('#mySelect').on('changed.bs.select', function (e) {
  // do something...
    //alert($(this).find("option:selected").text() );
    var host = window.location.host;
    $("#tengah").attr("src", "http://"+host+"/frontend/img/sim1-tengah-"+$(this).find("option:selected").text()+".png");
    
    $("#tulisan").attr("src", "http://"+host+"/frontend/img/sim1-pilihan-"+$(this).find("option:selected").text()+".png");
    
});


function pindahketiga(){
    var opsi = $('#mySelect').find("option:selected").text();
    opsi = opsi.toLowerCase();
   var url =  "/frontend/simulasi-2/"+opsi;
    
    window.location.href = url;
}




/* Math Function */

/* Based on 
 * - EGM Mathematical Finance class by Enrique Garcia M. <egarcia@egm.co>
 * - A Guide to the PMT, FV, IPMT and PPMT Functions by Kevin (aka MWVisa1)
 */

var ExcelFormulas = {

	PVIF: function(rate, nper) {
		return Math.pow(1 + rate, nper);
	},

	FVIFA: function(rate, nper) {
		return rate == 0? nper: (this.PVIF(rate, nper) - 1) / rate;
	},	

	PMT: function(rate, nper, pv, fv, type) {
		if (!fv) fv = 0;
		if (!type) type = 0;

		if (rate == 0) return -(pv + fv)/nper;
		
		var pvif = Math.pow(1 + rate, nper);
		var pmt = rate / (pvif - 1) * -(pv * pvif + fv);

		if (type == 1) {
			pmt /= (1 + rate);
		};

		return pmt;
	},

	IPMT: function(pv, pmt, rate, per) {
		var tmp = Math.pow(1 + rate, per);
		return 0 - (pv * tmp * rate + pmt * (tmp - 1));
	},

	PPMT: function(rate, per, nper, pv, fv, type) {
		if (per < 1 || (per >= nper + 1)) return null;
		var pmt = this.PMT(rate, nper, pv, fv, type);
		var ipmt = this.IPMT(pv, pmt, rate, per - 1);
		return pmt - ipmt;
	},
	
	DaysBetween: function(date1, date2) {
		var oneDay = 24*60*60*1000;
		return Math.round(Math.abs((date1.getTime() - date2.getTime())/oneDay));
	},
	
	// Change Date and Flow to date and value fields you use
	XNPV: function(rate, values) {
		var xnpv = 0.0;
		var firstDate = new Date(values[0].Date);
		for (var key in values) {
			var tmp = values[key];
			var value = tmp.Flow;
			var date = new Date(tmp.Date);
			xnpv += value / Math.pow(1 + rate, this.DaysBetween(firstDate, date)/365);
		};
		return xnpv;
	},

	FV: function(rate, nper, pmt, pv, type) {
        if (!type) type = 0;

        var pow = Math.pow(1 + rate, nper);
        var fv = 0;

        if (rate) {
            fv = (pmt * (1 + rate * type) * (1 - pow) / rate) - pv * pow;
        } else {
            fv = -1 * (pv + pmt * nper);
        }

        return fv;
    },
    
    PV: function(rate, nper, pmt, fv, type){
        if (!type) type = 0;

        var pow = Math.pow(1 + rate, nper);
        var pv = 0;

        pv = fv/pow;

        return pv;
    },
    
    RATES: function(nper, pmt, pv, fv, type, guess) 
    {
      if (guess == null) guess = 0.01;
      if (fv == null) fv = 0;
      if (type == null) type = 0;

      var FINANCIAL_MAX_ITERATIONS = 128;//Bet accuracy with 128
      var FINANCIAL_PRECISION = 0.0000001;//1.0e-8

      var y, y0, y1, x0, x1 = 0, f = 0, i = 0;
      var rate = guess;
      if (Math.abs(rate) < FINANCIAL_PRECISION) {
         y = pv * (1 + nper * rate) + pmt * (1 + rate * type) * nper + fv;
      } else {
         f = Math.exp(nper * Math.log(1 + rate));
         y = pv * f + pmt * (1 / rate + type) * (f - 1) + fv;
      }
      y0 = pv + pmt * nper + fv;
      y1 = pv * f + pmt * (1 / rate + type) * (f - 1) + fv;

      // find root by Newton secant method
      i = x0 = 0.0;
      x1 = rate;
      while ((Math.abs(y0 - y1) > FINANCIAL_PRECISION) && (i < FINANCIAL_MAX_ITERATIONS)) {
         rate = (y1 * x0 - y0 * x1) / (y1 - y0);
         x0 = x1;
         x1 = rate;

         if (Math.abs(rate) < FINANCIAL_PRECISION) {
            y = pv * (1 + nper * rate) + pmt * (1 + rate * type) * nper + fv;
         } else {
            f = Math.exp(nper * Math.log(1 + rate));
            y = pv * f + pmt * (1 / rate + type) * (f - 1) + fv;
         }

         y0 = y1;
         y1 = y;
         ++i;
      }
      return rate;}
};

function numberWithCommas(x) {
    var dummy = x.toString().replace(/[^\d\.\-\ ]/g,'');
    return dummy.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

$('#pv').change(function() {
    var pv = document.getElementById("pv").value;
  var nilai = numberWithCommas(pv);
    document.getElementById("pv").value = nilai;
});

function hitung(){
    var pv = document.getElementById("pv").value;
    pv = pv.replace(/[^\d\.\-\ ]/g,"");
    var nper = parseFloat($('#nper').find("option:selected").text())
    var as_infla = parseFloat($('#asinlfasi').find("option:selected").text()) / 100.0 ;
    var as_return = parseFloat($('#asreturn').find("option:selected").text()) / 100.0 ;
    
    var nilai_fv = Math.round(ExcelFormulas.FV(as_infla, nper,0,-1*pv,0));
    
   document.getElementById("val_FV").value =    numberWithCommas(nilai_fv);
    
    
    var nilai_pv = Math.round(ExcelFormulas.PV(as_return,nper,0,nilai_fv,0));
   document.getElementById("val_PV").value =    numberWithCommas(nilai_pv);
    
    var myrates = ExcelFormulas.RATES(12,0,-1,1+as_return,1);
    
    var nilai_pmt = Math.round(ExcelFormulas.PMT(myrates,12*nper,0,-1*nilai_fv,1));
   document.getElementById("val_PMT").value =    numberWithCommas(nilai_pmt);
    
}




function checkFV(){
    //alert('shit');
    var nilai = ExcelFormulas.FV(0.06, 5,0,1000000000,0);
    alert(nilai);
}

function checkPV(){
    var nilai = ExcelFormulas.PV(0.16,5,0, 1338225578,0 );
    alert(nilai);
}

function checkPMT(){
    var nilai = ExcelFormulas.PMT(0.0124451,60,0,-1338225578,1);
    alert(nilai);
}

function checkRate(){
    var nilai = ExcelFormulas.RATES(12,0,-1,1.16,1);
    alert(nilai);
}

//checkRate();